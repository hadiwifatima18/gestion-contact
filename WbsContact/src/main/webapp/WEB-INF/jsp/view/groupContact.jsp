
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<title>registration form</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
	crossorigin="anonymous">

<style>
h3 {
	margin-top: 20px;
}

#navbarNav div {
	height: 0;
}

#navbarNav form {
	margin: 0;
	padding: 0;
}

form {
	margin-bottom: 60px;
	margin-top: 10px;
	padding: 10px;
}
</style>

</head>
<body>
	<div class="container">

		 <nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link active"
						aria-current="page"
						href="${pageContext.request.contextPath}/showForm">Home</a></li>





					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/showForm">Add Contact
					</a></li>


					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/manageContacts">Manage
							Contact </a></li>

					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/creerNewGroup">New Group</a>
					</li>
					
					
					
			<li class="nav-item">
				<form action="${pageContext.request.contextPath}/searchContact" class="d-flex" method="POST">
        				<input name="searchType" type="text" class="form-control me-2" placeholder="nom/telephone/groupe" aria-label="Search type">
       					<input name="searchValue" type="text" class="form-control me-2" placeholder="Valeur de recherche" aria-label="Search value">
        				<button class="btn btn-outline-success" type="submit">Search</button>
				</form>
			</li>
			
		</ul>
	  </div>
    </nav>
  <div>
		
		
		
		
		
		
		
		
		
			<h3>Group Form</h3>
		    </div>
		
		    <div>
				<c:if test="${infoMsg!=null}">
					<div class="alert alert-success" role="alert">${infoMsg}</div>
					</c:if>
					<c:if test="${errorMsg!=null}">
						<div class="alert alert-danger" role="alert">${errorMsg}</div>
				</c:if>
				
				
			  	<table class="table table-striped">
						<thead>
						  <tr>
								<th scope="col">Nom</th>
								<th scope="col">Prenom</th>
								<th scope="col">Téléphone Professionnel</th>
								<th scope="col">Téléphone Personnel</th>
								<th scope="col">Email Professionnel </th>
								<th scope="col">Email Personnel </th>
								<th scope="col">Adresse </th>
								<th scope="col">Action</th>
							
							</tr>
							
						</thead>
					
		  			<c:forEach items="${contactList}" var="cL">
							<tr>
								<td><c:out value="${cL.nom}" /></td>
								<td><c:out value="${cL.prenom}" /></td>
								<td><c:out value="${cL.telephone2}" /></td>
								<td><c:out value="${cL.telephone1}" /></td>
								<td><c:out value="${cL.email2}" /></td>
								<td><c:out value="${cL.email1}" /></td>
								<td><c:out value="${cL.adresse}" /></td> 
								<td>
									
            					  <select multiple="multiple" class="form-control" id="groupes" name="groupes" size="5">
                					<option th:each="groupe : ${groupList}" th:value="${groupe.idGroup}" th:text="${groupe.nomGroup}"></option>
           						 </select>
           						 
           						<a href="affecterGroup/${groupList.idGroup}">Affecter</a>
								</td>
								
							<!-- ************************************************************ -->
				  <td>
					    <label for="liste des contacts"><h4>liste des contacts</h4></label>			
					    <select multiple="multiple" class="form-control" id="groupes " name="groupes " size="6">
					    
					        <c:forEach items="${groupList}" var="tableData"> -->
					           <!-- <option value="${tableData.idContact}">${tableData.nom}</option>  	-->           
					             <option value="${tableData.idGroup}">${tableData.nomGroup} 
					        </c:forEach>
					        
					    </select>
				</td>
				
							  </tr>
		
						</c:forEach>
		
				</table>
				
			
					
		</div>