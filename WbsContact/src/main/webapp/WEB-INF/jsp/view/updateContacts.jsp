
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<title>Update form</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
	crossorigin="anonymous">

<style>
h3 {
	margin-top: 20px;
}

#navbarNav div {
	height: 0;
}

#navbarNav form {
	margin: 0;
	padding: 0;
}

form {
	margin-bottom: 60px;
	margin-top: 10px;
	padding: 10px;
}
</style>

</head>
<body>
	<div class="container">

		 <nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link active"
						aria-current="page"
						href="${pageContext.request.contextPath}/showForm">Home</a></li>





					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/showForm">Add Contact
					</a></li>


					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/manageContacts">Manage
							Contact </a></li>

					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/creerNewGroup">New Group </a></li>
						
					
			<li class="nav-item">
				<form action="${pageContext.request.contextPath}/searchContact" class="d-flex" method="POST">
        				<input name="searchType" type="text" class="form-control me-2" placeholder="nom/telephone/groupe" aria-label="Search type">
       					<input name="searchValue" type="text" class="form-control me-2" placeholder="Valeur de recherche" aria-label="Search value">
        				<button class="btn btn-outline-success" type="submit">Search</button>
				</form>
			</li>
				</ul>
			</div>
		</nav>
		<div>
		
			<h3>Update form</h3>
		</div>
		
		<div>
		
			<f:form action="${pageContext.request.contextPath}/modifierContact" method = "POST" modelAttribute = "contactModel">
				<f:hidden path="idContact" />
		
				<div class="row">
					<div class="col">
						<label>Nom</label>
						<f:input path="nom" type="text" class="form-control"
							placeholder="Nom" />
						<f:errors path="nom" class="text-danger" />
					</div>

					<div class="col">
						<label>Prenom</label>
						<f:input path="prenom" type="text" class="form-control"
							placeholder="Prenom" />
						<f:errors path="prenom" class="text-danger" />
					</div>
				</div>


				<div class="row">
					<div class="col">
						<label>Téléphone Professionnel</label>
						<f:input path="telephone2" type="text" class="form-control"
							placeholder="Téléphone Professionnel" />
						<f:errors path="telephone2" class="text-danger" />
					</div>

					<div class="col">
						<label>Téléphone personnel</label>
						<f:input path="telephone1" type="text" class="form-control"
							placeholder="Téléphone personnel" />
						<f:errors path="telephone1" class="text-danger" />
					</div>
				</div>
				
				
				
				<div class="row">
					<div class="col">
						<label>Email Professionnel</label>
						<f:input path="email2" type="text" class="form-control"
							placeholder="Email Professionnel" />
						<f:errors path="email2" class="text-danger" />
					</div>

					<div class="col">
						<label>Email personnel</label>
						<f:input path="email1" type="text" class="form-control"
							placeholder="Email personnel" />
						<f:errors path="email1" class="text-danger" />
					</div>
				</div>
				
				
				<div class="row">
					<div class="col">
						<label>Address</label>
						<f:input path="adresse" type="text" class="form-control"
							placeholder="adresse" />
						<f:errors path="adresse" class="text-danger" />
					</div>

					<div class="col">
						<legend class="col-form-label col-sm-2 pt-0">Gender</legend>
						<div class="form-check">
							<f:radiobutton path="genre" class="form-check-input"
								value="Female" />
							<label class="form-check-label">Female </label>
						</div>
						<div class="form-check">
							<f:radiobutton path="genre" class="form-check-input"
								value="Male " />
							<label class="form-check-label">Male </label>
						</div>
						<f:errors path="genre" class="text-danger" />
						<%-- <form:radiobuttons path="abc" items="${xyz}"/>   --%>

					</div>

				</div>
				<div style="text-align: right">
					<button type="submit" class="btn btn-success">Modifier</button>
					<button type="reset" class="btn btn-danger">Annuler</button>
				</div>

			</f:form>
		</div>

		  <div>

			
		</div>
	</div>
</body>
</html>