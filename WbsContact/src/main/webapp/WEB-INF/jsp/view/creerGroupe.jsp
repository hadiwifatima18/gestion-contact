
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<%@ taglib prefix="f" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html>
<head>
<title>Update form</title>
<link
	href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css"
	rel="stylesheet"
	integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6"
	crossorigin="anonymous">

<style>
h3 {
	margin-top: 20px;
}

#navbarNav div {
	height: 0;
}

#navbarNav form {
	margin: 0;
	padding: 0;
}

form {
	margin-bottom: 60px;
	margin-top: 10px;
	padding: 10px;
}
</style>

</head>
<body>
	<div class="container">

		 <nav class="navbar navbar-expand-lg navbar-light bg-light">
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item"><a class="nav-link active"
						aria-current="page"
						href="${pageContext.request.contextPath}/showForm">Home</a></li>





					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/showForm">Add Contact
					</a></li>


					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/manageContacts">Manage
							Contact </a></li>

					<li class="nav-item"><a class="nav-link"
						href="${pageContext.request.contextPath}/creerNewGroup">New Group  </a></li>
						
				
			<li class="nav-item">
				<form action="${pageContext.request.contextPath}/searchContact" class="d-flex" method="POST">
        				<input name="searchType" type="text" class="form-control me-2" placeholder="nom/telephone/groupe" aria-label="Search type">
       					<input name="searchValue" type="text" class="form-control me-2" placeholder="Valeur de recherche" aria-label="Search value">
        				<button class="btn btn-outline-success" type="submit">Search</button>
				</form>
			</li>			
		</ul>
			</div>
		</nav>
		<div>
		
			<h3>Créer un nouveau groupe </h5>
		</div>
		
	<div>
			<c:if test="${infoMsg!=null}">
					<div class="alert alert-success" role="alert">${infoMsg}</div>
				</c:if>
				<c:if test="${errorMsg!=null}">
					<div class="alert alert-danger" role="alert">${errorMsg}</div>
				</c:if>
			
				
			 <f:form action="${pageContext.request.contextPath}/addGroup" method="POST" modelAttribute="groupModel">
				 <div class="row">
					  <div class="mb-3">
					    <label for="exampleInputEmail1" class="form-label">Nom : </label>
					    <input class="form-control" placeholder="Nom de groupe" name="nomGroup">
					    <f:errors path="nomGroup" class="text-danger" />
					  </div>
				 </div>	
				
             <!-- <p>Nombre de contacts : ${contactList.size()}</p> -->

				 
			<div>
			    <label for="liste des contacts"></label>
			    
			    <select multiple="multiple" class="form-control" id="contacts" name="contactIds" size="6">
			    
			        <c:forEach items="${contactList}" var="tableData">
			           <!-- <option value="${tableData.idContact}">${tableData.nom}</option>  	-->           
			            <option value="${tableData.idContact}">${tableData.prenom} ${tableData.nom} - ${tableData.telephone1} - ${tableData.telephone2}
			        </c:forEach>
			        
			    </select>
			</div>
				 
				 <div>
				 
				 
				 </div>
		 		<br>
				 <div class="row"> 
					  <button type="submit" class="btn btn-success">Ajouter</button>
				</div>
			 </f:form>
			 
			 
			 
		<div>
		<h3>Liste des groupes</h3>

			<table class="table table-striped">
				    <thead>
				        <tr>
				            <th scope="col">Nom de Groupe</th>
				            <th scope="col">Nombre de membres</th>
				            <th scope="col">Actions</th>
				        </tr>
				    </thead>
				    <tbody>
				        <c:forEach items="${groupList}" var="gL">
				            <tr>
				                <td><c:out value="${gL.nomGroup}" /></td>
				                 <td><c:out value="${gL.contacts.size()}" /></td>
				                <td><a href="deleteGroup/${gL.idGroup}">Delete</a></td>
				            </tr>
				        </c:forEach>
				    </tbody>
			</table>

		</div>	 
			 
			 
						
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
			 
		  
	</div>
</body>
</html>