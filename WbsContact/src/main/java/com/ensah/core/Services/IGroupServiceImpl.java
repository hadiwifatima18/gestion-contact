package com.ensah.core.Services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ensah.core.Dao.IGroupDao;
import com.ensah.core.bo.Contact;
import com.ensah.core.bo.Groupe;


@Service
public class IGroupServiceImpl implements IGroupService {
	@Autowired

	private IGroupDao groupDao;
	@Override
	public void addGroupe(Groupe pGroup) {
		groupDao.save(pGroup);
	}

	@Override
	public void deleteGroup(Long id) {
		groupDao.deleteById(id);
		
	}

	@Override
	public List<Groupe> getAllGroups() {
		
		 return groupDao.findAllGroupsOrderNom();
	}
	
	//
	@Override
	public List<Groupe> getGroupByName(String nomGroup) {
		
		 return groupDao.ChercherGroupByName(nomGroup);
	}

	/*@Override
	public List<Groupe> getGroupesByIds(Long[] groupeIds)  {
		
		return groupDao.chercherGroupByIdIn(groupeIds);
	}*/

	@Override
	public Groupe findGroupeByIdGroup(Long id) {
		// TODO Auto-generated method stub
		return groupDao.findGroupeByIdGroup(id);

	}
	
	

}
