package com.ensah.core.Services;


import java.util.List;

import com.ensah.core.bo.Contact;
import com.ensah.core.bo.Groupe;

public interface IGroupService {
	
	//1.Creer un Groupe
	
	public void addGroupe(Groupe pGroup);
	
	//2.Supprimer un Groupe
	public void deleteGroup(Long idGroup);
	
	// 3.Afficher la liste des groupes par ordre alphabetique
	
		public List<Groupe> getAllGroups();
		
	
	//chercher les ids des groupes selectionnées 
		

		//public List<Groupe> getGroupesByIds(Long[] groupeIds);
		public Groupe findGroupeByIdGroup(Long id);

		
		
	//Rechercher un groupe par nom
		
		public List<Groupe> getGroupByName(String nomGroup);

		
}
