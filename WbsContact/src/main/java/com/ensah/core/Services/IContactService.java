package com.ensah.core.Services;

import java.util.List;

import com.ensah.core.bo.Contact;



public interface IContactService {
	
	// 1.Creer un nouveau contact
	
	public void addContact(Contact pContact);


	// 2.Afficher la liste des contacts par ordre alphabetique
	
	public List<Contact> getAllContacts();
	//public List<Contact> getAllContacts(String nom);
	
	// 3.Supprimer un contact

	public void deleteContact(Long id);
	
	
	// 4.Modifier un contact
	
	public void updateContact(Contact pContact);
	
	// 5.Rechercher un contact par nom

	public List<Contact> getContactByNom(String nom);
	
	
	//6.Rechercher un contact par numero de telephone
	
	public List<Contact> getContactByTelephone(String tel);

	// Rechercher un contact par Id
	
	public Contact getContactById(Long idContact);


	
	//chercher les ids des contactes selectionnées 
	public List<Contact> getContactsByIds(Long[] contactIds);
}
