package com.ensah.core.Services;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ensah.core.Dao.IContactDao;
import com.ensah.core.bo.Contact;



@Service
@Transactional
public class ContactServiceImpl implements IContactService{
	
	@Autowired
	private IContactDao  ContactDao;
	
	

	// 1.Creer un nouveau contact
	
	@Override
	public void addContact(Contact pContact) {
		 
		ContactDao.save(pContact);
		
	}

	// 2.Afficher la liste des contacts par ordre alphabetique
	
	@Override
	public List<Contact> getAllContacts(){
		
		return ContactDao.findAllContactsOrderNom();
	}
	
	
	//3.Supprimer un contact
	
	@Override
	public void deleteContact(Long id) {
		
		ContactDao.deleteById(id);
		
	}
	
	
	
	
	// 4.Modifier un contact
	
	@Override
	public void updateContact(Contact pContact) {

		ContactDao.save(pContact);
	}

	// 5.Rechercher un contact par nom
	
	@Override
	public List<Contact> getContactByNom(String nom) {
		
		return ContactDao.ChercherContactByNom(nom);
	}
	
	
	// 6.Rechercher un contact par numero de telephone
	
	@Override
	public List<Contact> getContactByTelephone(String tel) {
		
		 return ContactDao.ChercherContact(tel);
	}

	@Override
	public Contact getContactById(Long idContact) {
		
		return ContactDao.findById(idContact).get();
	}
	
	@Override
    public List<Contact> getContactsByIds(Long[] contactIds) {
        return ContactDao.chercherByIdIn(contactIds);
    	
    }

}
