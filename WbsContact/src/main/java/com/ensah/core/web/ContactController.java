package com.ensah.core.web;

import javax.servlet.ServletContext;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.ensah.core.Services.IContactService;
import com.ensah.core.Services.IGroupService;
import com.ensah.core.bo.Contact;
import com.ensah.core.bo.Groupe;


@Controller	
public class ContactController {
	
	//protected final Logger LOGGER = Logger.getLogger(getClass());
	//protected final Logger LOGGER = Logger.getLogger(ContactController.class);

	@Autowired
	private IContactService  contactService;
	
	@Autowired
	private IGroupService groupService;
	
	@Autowired
	private ServletContext appContext;
	
	
	@GetMapping({"/","/showForm"})
	
	public String ShowForm(Model model) {
		
		model.addAttribute("groupList", groupService.getAllGroups());
 		List<Groupe> groups = groupService.getAllGroups();
		
		 //Ajouter un objet Contact vide comme attribut du modèle
		 model.addAttribute("contactModel", new Contact());
		
		
		
		// Ajouter la liste des personnes comme attribut du modèle
		model.addAttribute("contactList", contactService. getAllContacts());
		
		
		return "Form";
	}
	
	@PostMapping("/showForm")
	public String  CreerContact(@Valid @ModelAttribute("contactModel")Contact pContact,BindingResult bindingResult,
			ModelMap model) {
		
		//model.addAttribute("groupeModel", new Groupe());

		if (bindingResult.hasErrors()) {
			model.addAttribute("errorMsg", "Les données sont invalides.");
			//LOGGER.warn("Erreur de validation du formulaire");
		} else {
			contactService.addContact(pContact);
			model.addAttribute("infoMsg", "Contact ajouté avec succès");

		}

		return "Form";
			
	}
	
	/*-----------------------------------------------
	 * 				
	 * 				Ajouter Contact
	 * 
     -----------------------------------------------*/
	
	@RequestMapping("/addContact")
	public String saveContact(@Valid @ModelAttribute("contactModel")Contact pContact, BindingResult bindingResult,ModelMap model) {
		

		if (bindingResult.hasErrors()) {
			model.addAttribute("errorMsg", "Les données sont invalides.");
		} else {
			contactService.addContact(pContact);
			model.addAttribute("infoMsg", "Contact ajouté avec succès");

		}

		// Mettre la liste des personnes dans le modèle	
		model.addAttribute("contactList", contactService.getAllContacts()); 

		return "Form";

	}
	
	
	
	@GetMapping("/manageContacts")
	public String manageContact(Model model) {
		model.addAttribute("groupList", groupService.getAllGroups());
 		List<Groupe> groups = groupService.getAllGroups();
	    System.out.println("fff");
	 // Mettre la liste des personnes dans le modèle	
	 		model.addAttribute("contactList", contactService.getAllContacts());
	    return "manageContacts";
	}

	
	
	
	/*-----------------------------------------------
	 * 				
	 * 				Delete Contact
	 * 
     -----------------------------------------------*/
	
	
	
	@RequestMapping("/deleteContact/{idContact}")
	public String delete(@PathVariable int idContact,Model model) {
	    contactService.deleteContact(Long.valueOf(idContact));
	    System.out.println("cccccccccccccccccccccccc");
	   // model.addAttribute("contactList", contactService.getAllContacts());
	    //return "manageContacts";
	    return "redirect:/manageContacts";
	}
	
	/*--------------------------------------------------------
	 * 				
	 * 					Update Contact
	 * 
	 * --------------------------------------------------------*/
	
	  
	
	@RequestMapping(value = "/updateContactForm/{idContact}", method = RequestMethod.GET)
	public String update(@PathVariable(name = "idContact") int idContact, Model model) {
	    model.addAttribute("contactModel", contactService.getContactById(Long.valueOf(idContact)));
	    return "updateContacts";
	}

	@RequestMapping("/modifierContact")
	public String updateContacts(@Valid @ModelAttribute("contactModel") Contact pContact, BindingResult bindingResult, Model model) {
	    
		model.addAttribute("groupList", groupService.getAllGroups());
 		List<Groupe> groups = groupService.getAllGroups();
		// Si il y a des erreurs de validation
	    if (bindingResult.hasErrors()) {
	        return "updateContacts";
	    }

	    // Si il n'y a pas d'erreurs
	    contactService.addContact(pContact);
	    // Mettre la liste des Contacts dans le modèle	
	    model.addAttribute("contactList", contactService.getAllContacts());

	    // rediriger vers un autre handler
	    return "redirect:/manageContacts";
	}
	
	/*--------------------------------------------------------
	 * 				
	 * 					Search Contact
	 * 
	 * --------------------------------------------------------*/
	
	
	@PostMapping(value = "searchContact")
	public String searchContact(@RequestParam String searchType, @RequestParam String searchValue, Model model) {
	    if (searchType.equalsIgnoreCase("groupe")) {
	        List<Groupe> listGroupes = groupService.getGroupByName(searchValue);
	        // Initialiser le modèle de groupe 
	        System.out.println(listGroupes);
	        model.addAttribute("groupList", listGroupes);
	        return "listGroup";
	    } else {
	        List<Contact> listContacts = new ArrayList<>();

	        if (searchType.equalsIgnoreCase("nom")) {
	            listContacts = contactService.getContactByNom(searchValue);
	        } else if (searchType.equalsIgnoreCase("telephone1")) {
	            listContacts = contactService.getContactByTelephone(searchValue);
	        }

	        // Initialiser le modèle
	        model.addAttribute("contactList", listContacts);
	        return "listContact";
	    }
	}

	
	/*--------------------------------------------------------
	 * 				
	 * 					Creer Groupe
	 * 
	 * --------------------------------------------------------*/
	
	@GetMapping("/creerNewGroup")
	public String manageGroupe(Model model) {
	    System.out.println("aaaa");
	    
	 // Ajouter un objet Groupe vide comme attribut du modèle
	 	model.addAttribute("groupModel", new Groupe());
	 	model.addAttribute("contactList", contactService.getAllContacts());
	 	System.out.println("bonjouuuuuuuuuuur"+contactService.getAllContacts());
	 	model.addAttribute("groupList", groupService.getAllGroups());
	    return "creerGroupe";
	}
		
	

	/*------------------------------------------------------------------------
	 * 				
	 * 	   ajouter des contactes à un groupe crée et Afficher List Groupe
	 * 
	 * ------------------------------------------------------------------------*/
	
	
	@PostMapping(value = "addGroup")
	public String  creergroupe(@Valid @ModelAttribute("groupModel")Groupe pGroup,ModelMap model,BindingResult bindingResult,@RequestParam("contactIds") Long[] contactIds) {
		
		//model.addAttribute("groupeModel", new Groupe());

			if (bindingResult.hasErrors()) {
				model.addAttribute("errorMsg", "Les données sont invalides.");
				System.out.println("11111111111111111");
				//LOGGER.warn("Erreur de validation du formulaire");
			} else {
				groupService.addGroupe(pGroup);
				System.out.println("222222222222222222222");
				model.addAttribute("infoMsg", "Groupe ajouté avec succès");
				
				
				List<Contact> contacts = contactService.getContactsByIds(contactIds);
			    for (Contact contact : contacts) {
			    	
			    	//ajoute le groupe actuel (pGroup) à la liste des groupes du contact.
			        contact.getGroupes().add(pGroup);
				    System.out.println("11111111111111");
				    System.out.println(contact.toString());
				    System.out.println("11111111111111");

				    //met à jour le contact pour prendre en compte les modifications apportées à la liste des groupes.
			        contactService.addContact(contact);
			        
			        //ajoute le contact à la liste des contacts du groupe actuel (pGroup).
			        pGroup.getContacts().add(contact);
				    System.out.println("55555555555");

				    System.out.println(pGroup.toString());
				    System.out.println("55555555555");

			       
			    }
				
				
				
	
				
			}	
				// model.addAttribute("contactList", contactService.getAllContacts());
				// Mettre la liste des personnes dans le modèle	
		 		
		 		
		 		System.out.println("Salaaaam"+ groupService.getAllGroups());
		 		

				model.addAttribute("groupList", groupService.getAllGroups());
		 		List<Groupe> groups = groupService.getAllGroups();
		 		model.addAttribute("contactList", contactService.getAllContacts());

				
			 	
	
		//return "creerGroupe";	
		 //return "groupContact"; 
		 return "listGroup";
		
	}
	

	/*@PostMapping(value = "listGroup")
    public String affecterGroupe(@RequestParam("groupes") Long[] groupeIds, @RequestParam("contactIds") Long[] contactIds) {
		
		List<Contact> contacts = contactService.getContactsByIds(contactIds);
	    List<Groupe> groupes = groupService.getGroupesByIds(groupeIds);
	    for (Contact contact : contacts) {
	        contact.getGroupes().addAll(groupes);
	        contactService.addContact(contact);  
	    }
	    
	    System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++");
	    System.out.println("fatimaaaaaaaaaaaaaaaaaam");
	    return "HHH";
    }*/
	
	
	/*-----------------------------------------------
	 * 				
	 * 				Delete Group
	 * 
     -----------------------------------------------*/
	
	
	
	@RequestMapping("/deleteGroup/{idGroup}")
	public String supprimerGroup(@PathVariable int idGroup,Model model) {
		groupService.deleteGroup(Long.valueOf(idGroup));
	    System.out.println("qqqqqqqqqqqqqqqq");
	  
	    
	    return "redirect:/creerNewGroup";
	   
	}
	
	
	
	
	
	
	
	
	
	
	
 }
