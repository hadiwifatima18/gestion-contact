package com.ensah.core.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.ensah.core.bo.Contact;

public interface IContactDao  extends JpaRepository<Contact, Long> {
	
	

	@Query("Select C from Contact C order by C.nom ")
	//public List<Contact> findAllContactsOrderNom(@Param("nom") String nom);
	public List<Contact> findAllContactsOrderNom();
	
	
	
	@Query("Select C from Contact C where C.nom = :nom")
	public List<Contact> ChercherContactByNom(@Param("nom") String nom);

	@Query("Select C from Contact C where C.telephone1 = :tel1 OR C.telephone2 = :tel1")
	public List<Contact> ChercherContact(@Param("tel1")String tel);
	
	
	
	
	
	@Query("SELECT c FROM Contact c WHERE c.idContact IN :contactIds")
	List<Contact> chercherByIdIn(Long[] contactIds);
			
			
		
	
	
	
	
	


}
