package com.ensah.core.Dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.ensah.core.bo.Contact;
import com.ensah.core.bo.Groupe;

@Repository
public interface IGroupDao extends JpaRepository<Groupe, Long>  {

	@Query("Select G from Groupe G order by G.nomGroup ")
	public List<Groupe> findAllGroupsOrderNom();
	
	
	
	@Query("SELECT g FROM Groupe g WHERE g.idGroup IN :groupeIds")
	List<Groupe> chercherGroupByIdIn(Long[] groupeIds);
	Groupe findGroupeByIdGroup(Long id);
	
	
	@Query("Select G from Groupe G where G.nomGroup = :nomGroup ")
	public List<Groupe>ChercherGroupByName(String nomGroup);
	
}
