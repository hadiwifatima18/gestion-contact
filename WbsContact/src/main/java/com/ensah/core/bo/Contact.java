package com.ensah.core.bo;

import java.util.ArrayList;
import java.util.List;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;



@Entity
@Table
public class Contact {
	
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idContact;
	
	
	
	@ManyToMany
    @JoinTable(
            name = "contact_groupe",
            joinColumns= @JoinColumn(name = "contact_id"),
            inverseJoinColumns = @JoinColumn(name = "groupe_id")
    )	
	private List<Groupe> groupes = new ArrayList<>();
	
	@NotBlank(message = "This field is required")
	private String nom;
	
	@NotBlank(message = "This field is required")
	private String prenom;
	
	@Pattern(regexp = "^06\\d{8}$", message = "Le numéro de téléphone doit commencer par 06 et contenir 10 chiffres.")
	@NotBlank(message = "This field is required")
	@Column(name="Tel_personnel")
	private String telephone1;
	
	@Pattern(regexp = "^06\\d{8}$", message = "Le numéro de téléphone doit commencer par 06 et contenir 10 chiffres.")
	@NotBlank(message = "This field is required")
	@Column(name="Tel_professionnel")
	private String telephone2;
	
	@NotBlank(message = "This field is required")
	private String adresse;
	
	@Email(message = "Enter a valid email")
	@NotBlank(message = "This field is required")
	@Column(name="Email_personnel")
	private String email1;
	
	@Email(message = "Enter a valid email")
	@NotBlank(message = "This field is required")
	@Column(name="Email_professionnel")
	private String email2;
	
	
	private Genre genre;
	
	
	/****************************
	 * 
	 * Getters and Setters
	 * 
	 * 
	 * **************************/
	


	/****
	 * 
	 * Pour afficher 
	 * 
	 * *****/
	
	
	@Override
	public String toString() {
		return "Contact [IdContact=" + idContact + ", Nom=" + nom + ", Prenom=" + prenom + ", Telephone1=" + telephone1
				+ ", Telephone2=" + telephone2 + ", Adresse=" + adresse + ", Email1=" + email1 + ", Email2=" + email2
				+ ", genre=" + genre + "]";
	}
	
	
	
	public Long getIdContact() {
		return idContact;
	}



	public void setIdContact(Long idContact) {
		this.idContact = idContact;
	}



	public List<Groupe> getGroupes() {
		return groupes;
	}



	public void setGroupes(List<Groupe> groupes) {
		this.groupes = groupes;
	}



	public String getNom() {
		return nom;
	}



	public void setNom(String nom) {
		this.nom = nom;
	}



	public String getPrenom() {
		return prenom;
	}



	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}



	public String getTelephone1() {
		return telephone1;
	}



	public void setTelephone1(String telephone1) {
		this.telephone1 = telephone1;
	}



	public String getTelephone2() {
		return telephone2;
	}



	public void setTelephone2(String telephone2) {
		this.telephone2 = telephone2;
	}



	public String getAdresse() {
		return adresse;
	}



	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}



	public String getEmail1() {
		return email1;
	}



	public void setEmail1(String email1) {
		this.email1 = email1;
	}



	public String getEmail2() {
		return email2;
	}



	public void setEmail2(String email2) {
		this.email2 = email2;
	}



	public Genre getGenre() {
		return genre;
	}



	public void setGenre(Genre genre) {
		this.genre = genre;
	}



	/************************
	 * constructeur vide
	 * 
	 * ********************/
	
	public Contact() {
		
	}
	
	
	
	
	
	
	
	
}