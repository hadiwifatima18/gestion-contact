package com.ensah.core.bo;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

@Entity
@Table
public class Groupe {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long idGroup;
	
	
	@ManyToMany
    @JoinTable(
            name = "contact_groupe",
            joinColumns= @JoinColumn(name = "groupe_id"),
            inverseJoinColumns = @JoinColumn(name = "contact_id")
            )
	
	private List<Contact> contacts = new ArrayList<>();
	
	@NotBlank(message = "This field is required")
	private String nomGroup;

	public Long getIdGroup() {
		return idGroup;
	}

	public void setIdGroup(Long idGroup) {
		this.idGroup = idGroup;
	}

	public String getNomGroup() {
		return nomGroup;
	}

	public void setNomGroup(String nomGroup) {
		this.nomGroup = nomGroup;
	}
	
	/**
	 * Afficher 
	 */

	@Override
	public String toString() {
		return "Groupe [IdGroup=" + idGroup + ", NomGroup=" + nomGroup + "]";
	}
	
	public Groupe() {
		
	}

	public List<Contact> getContacts() {
		return contacts;
	}

	public void setContacts(List<Contact> contacts) {
		this.contacts = contacts;
	}
	
	
	
	
	
	
	

}
